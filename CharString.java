import java.util.Scanner;    // Input keyboard

// Deanu Hararatinu Tu'u

public class CharacterizedString{
	public static void main(String[] args){

	   Scanner input = new Scanner(System.in);
           System.out.print("Masukkan kata/kalimat, kemudian enter: ");
           String kalimat = input.nextLine();

	   // Menguraikan kalimat
           System.out.println("Uraian setiap hurufnya adalah: ");
	   for(int i = 0; i < kalimat.length(); i++){
		int j = i + 1;
		System.out.print("Karakter ke-"+ j + ": " + kalimat.charAt(i) + "\n");
	   }

   	}
}